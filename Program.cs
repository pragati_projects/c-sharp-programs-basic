﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace SampleArrayList
{
    class Program
    {
        public List<Object> CreateInteger(List<Object> basic)
        {
          
            int number = 0;
          
                Console.WriteLine("Enter the a number:");
                number = Convert.ToInt32(Console.ReadLine());
             
           
            Console.WriteLine("ADDED SUCCESSFULLY");
            basic.Add(number);
            return basic;
        }

        public void DisplayBasic(List<Object> basic)
        {
            foreach(Object basic1 in basic)
            {
                Console.WriteLine("Numbers are:"+" " + basic1);
            }
        }

        public void Menu()
        {
            Console.WriteLine("Enter Integers:");
            Console.WriteLine("Enter 2 to display");
            Console.WriteLine("Enter 3 to Exit");
        }
        static void Main(string[] args)
        {
            List<Object> basic= new List<Object>();
            Program obj1 = new Program();
            int choice;
            do
            {
                obj1.Menu();

                Console.WriteLine("Enter your choice:");
                choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                       obj1.CreateInteger(basic);
                        break;

                    case 2:
                        obj1.DisplayBasic(basic);
                        Console.WriteLine("count is:" + basic.Count);
                        Console.WriteLine("capacity is" + basic.Capacity);
                        
                        int sum = 0;
                        foreach (int obj in basic)
                            sum = sum + obj;
                       
                        double average = sum / basic.Count;
                        Console.WriteLine("Sum is:" + sum);
                        Console.WriteLine("Average is:" + average);

                        if (basic.Count % 2 == 0)
                        {
                            int midPosition = basic.Count / 2;
                            basic.Insert(midPosition, average);
                        }
                        else
                        {
                            int midPosition = (basic.Count / 2) + 1;
                            basic.Insert(midPosition, average);

                        }

                        Console.WriteLine("Modified list");
                        for (int i = 0; i < basic.Count; i++)
                        {
                            Console.WriteLine(basic[i]);
                        }

                        basic.RemoveAt(2);
                        Console.WriteLine("After deleting");
                        foreach(Object obj in basic)
                        {
                            Console.WriteLine(obj);
                        }

                        basic.Remove(12);
                        Console.WriteLine("After deleting");
                       foreach(Object obj in basic)
                        {
                            Console.WriteLine(obj);
                        }

                        Console.WriteLine(basic.Contains(12));
                        Console.WriteLine(basic.Contains(54));



                        break;

                    case 3:
                        Console.WriteLine("EXITING");
                        break;
                }


            } while (choice != 3);



        }
    }
}
